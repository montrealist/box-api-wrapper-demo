const boxApiWrapper = require('@hmh/box-api-wrapper');

const link = 'https://hmhco.box.com/s/rps2uqhug95snio3r7mp1re319ko87r4';
const path = '.';
const params = {
    extensionFilter: '.ai',
    useManifest: false,
    validateFilenamesForHabitat: true,
    forceLowerCasePaths: true
};

(async () => {
    await boxApiWrapper.download(link, path, params);
    console.log('download finished.');
})();